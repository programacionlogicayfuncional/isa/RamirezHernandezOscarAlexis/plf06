(ns plf06.core
  (:gen-class))

(def chars-1
  "AÁBCDEÉFGHIÍJKLMNÑOÓPQRSTUÚÜVWXYZaábcdeéfghiíjklmnñoópqrstuúüvwxyz01234!\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~56789")
(def chars-2
  "KLMNÑOÓPQRSTUÚÜVWXYZaábcdeéfghiíjklmnñoópqrstuúüvwxyz01234!\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~5678901234!\"#$%&'(")

(defn diccionario
  [xs ys]
  (zipmap xs ys))

(defn convertir-valor
  [ys]
  (get (diccionario chars-1 chars-2) ys " "))

(defn rot-13
  [xs]  (cond
          (= (first (rest xs)) \space) (apply str (first xs) (apply str (map convertir-valor (rest xs))))
          (contains? (into #{} xs) \space) (apply str (map convertir-valor xs))
          :else
          (apply str (map convertir-valor (apply str (subs xs 0 (- (count xs) 3)) " " (subs xs (- (count xs) 3) (count xs)))))))

(rot-13 "Canción #72")
(rot-13 (apply str "Canción" "#72"))
(rot-13 (apply str "Can" "ción" "#72"))
(rot-13 (apply str "Can" "ción" "#" "72"))
(rot-13 "c AN CIÓN # 7 2")

(defn -main
  [& args]
  (if (empty? args)
    (println "Ingresa al menos un caracter")
    (println (rot-13 (apply str args)))))

